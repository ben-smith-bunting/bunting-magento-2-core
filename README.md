## Bunting Core (Magento 2)

This module provides most of the functionality needed to use Bunting features within your Magento 2 store.

By itself, this module won't do anything - you **must** install one (or more) of the following submodules to access the full functionality of the Bunting platform:

* [Bunting personalisation](https://bitbucket.org/bunting-software/bunting-magento-2-personalisation)
* More coming _**very**_ soon 

## Installation

This module uses composer for installation, and does not need to be directly required as it is a dependency of all the child modules.

## Support

Bunting Core (and all submodules) officially support PHP 5.5+ and PHP 7.x, with support for Magento version 2.0+